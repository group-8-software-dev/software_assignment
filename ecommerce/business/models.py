from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
import datetime

# Create your models here.
class Business(models.Model):
  user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)   
  name =models.CharField(max_length=200)
  business_contact =  models.CharField(max_length=200)
  email = models.EmailField(blank=True)
  location = models.CharField(max_length=200)
  description = models.TextField()
  logo = models.ImageField(upload_to='logos/')

  def __str__(self):
    return self.name

class Product(models.Model):
    CATEGORIES = [
        ('electronics', 'Electronics'),
        ('fashion_beauty', 'Fashion and Beauty'),
        ('home_living', 'Home and Living'),
        ('outdoor', 'Out Door'),
        ('sporting', 'Sporting'),
        ('kids_corner', 'Kids Corner'),
        ('groceries', 'Groceries'),
    ]
    Business = models.ForeignKey(Business, on_delete=models.CASCADE, default=1)
    product_name = models.CharField(max_length=100)
    price = models.IntegerField()
    Description = models.TextField()
    availability = models.BooleanField(default=1)
    photo = models.ImageField(upload_to='photos/')
    category = models.CharField(max_length=255, choices=CATEGORIES, default=1)

    discount = models.BooleanField(default=False, blank= True)
    discounted_price = models.IntegerField(default=None,null=True,blank=True)
    
    def __str__(self):
        return self.product_name
    
    def get_rating(self):
        reviews_total=0
        for review in self.reviews.all():
            reviews_total += review.rating
        if reviews_total>0:
            return reviews_total / self.reviews.count() 
        return 0     
    
class Category(models.Model):
    CATEGORIES = [
        ('electronics', 'Electronics'),
        ('fashion_beauty', 'Fashion and Beauty'),
        ('home_living', 'Home and Living'),
        ('outdoor', 'Out Door'),
        ('sporting', 'Sporting'),
        ('kids_corner', 'Kids Corner'),
        ('groceries', 'Groceries'),
        ('skin', 'Skin'),
        ('nails', 'Nails'),
        ('hair', 'Hair'),
    ]

    name = models.CharField(max_length=255, choices=CATEGORIES, default=1)

     
    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = 'Categories'
    
    
class Service_category(models.Model):
   name = models.CharField(max_length=100)
   def __str__(self):
        return self.name
   class Meta:
        verbose_name_plural = 'Service_categories'
      
    
class Service(models.Model):
    Business = models.ForeignKey(Business, on_delete=models.CASCADE, default=1)
    service_name = models.CharField(max_length=100)
    description = models.TextField(default=1,null=True,blank=True)
    category = models.ForeignKey(Service_category,on_delete=models.CASCADE, default=1)
    photo = models.ImageField(upload_to='photos/', default='ecommerce/static/no-image.jpeg')
    price = models.IntegerField(default='0')
    opening_time = models.TimeField()
    closing_time = models.TimeField()
    location = models.CharField(max_length=100)

    def __str__(self):
        return self.service_name
    
    def get_rating(self):
        reviews_total=0
        for review in self.reviews.all():
            reviews_total += review.rating
        if reviews_total>0:
            return reviews_total / self.reviews.count() 
        return 0     
    
    
class Customer(models.Model):
    name = models.CharField(max_length=50)
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)    
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=100)    
    Address = models.CharField(max_length=150, null=True,blank=True )

    def __str__(self):
	    return self.name
    
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length= 10)
    email = models.EmailField(max_length=100)
    date_created = models.DateTimeField(User, default=datetime.datetime.today)
    residence = models.CharField(max_length=150)

    def __str__(self):
        return self.user.username
    
def create_profile(sender, instance, created, **kwargs):
         if created:
              user_profile = Profile(user=instance)
              user_profile.save()

post_save.connect(create_profile,sender=User)

    
class Order(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    Business = models.ForeignKey(Business, on_delete=models.CASCADE, default='1')
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    quantity = models.IntegerField(default=1)
    address = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)
    email = models.EmailField(max_length=100)  
    date = models.DateField(default=datetime.datetime.today)
    status = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.product} order"
    
    class Meta:
        ordering=['-date', '-status']
    
    def get_total_cost(self):
        return sum(item.get_cost() for item in self.items.all())
    

from django.utils import timezone

class Appointment(models.Model):
    Business = models.ForeignKey(Business, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE) 
    service =  models.ForeignKey(Service, on_delete=models.CASCADE)  
    date = models.DateField()  
    time = models.TimeField()  
    phone = models.CharField(max_length=20, default='', blank=True)
    email = models.EmailField(max_length=100)
    status = models.BooleanField(default=False)  
    created_at = models.DateField(default=datetime.datetime.today)    
    
    
    class Meta:
        ordering = ['date', 'time']  
        
    def __str__(self):
        return f"{self.service} Appointment"     
    
class Contact_us(models.Model):
    first_name = models.CharField(max_length=100,null= True)
    last_name = models.CharField(max_length=100, null= True)
    email = models.EmailField()
    mobile = models.CharField(max_length=20, null= True)
    message = models.TextField()

    def __str__(self):
        return f"{self.first_name} {self.last_name}"    

    

class Review(models.Model):
    product=models.ForeignKey(Product,related_name='reviews',on_delete=models.CASCADE,null=True)
    service =  models.ForeignKey(Service,related_name='reviews', on_delete=models.CASCADE,null=True)    
    rating=models.IntegerField(default=3)
    content=models.TextField()
    created_by=models.ForeignKey(User,related_name='reviews',on_delete=models.CASCADE)
    created_at=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.product} rating "
    

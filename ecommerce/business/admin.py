from django.contrib import admin
from . models import *

# Register your models here.
admin.site.register(Business)
admin.site.register(Product)
admin.site.register(Service)
admin.site.register(Service_category)
admin.site.register(Customer)
admin.site.register(Order)
admin.site.register(Profile)
admin.site.register(Category)
admin.site.register(Contact_us)
admin.site.register(Appointment)
admin.site.register(Review)



from django.forms import ModelForm
from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm, SetPasswordForm
from django.contrib.auth.models import User

from django.forms.widgets import DateTimeInput


from .models import *

class CreateUserForm(UserCreationForm):    
    class Meta:
        model = User
        fields = ['username', 'email', 'password1', 'password2']    

class Business_Form(ModelForm):
    class Meta:
        model = Business
        fields ="__all__"
        exclude=['user']

class Customer_Form(ModelForm):
    class Meta:
        model = Customer
        fields = "__all__"
        exclude=['user']

class UpdateProfileForm(UserChangeForm):
    password= None
    email = forms.EmailField(label="",  widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter E-mail'}))
    first_name = forms.CharField(label="",max_length=150, widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter first name'}))
    last_name = forms.CharField(label="",max_length=150, widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter last name'}))
       
    class Meta:
        model = User
        fields = ['email','first_name','last_name' ]

class UpdatePasswordForm(SetPasswordForm):
    new_password1 = forms.CharField(label="",max_length=150, widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Enter new password', }))
    new_password2 = forms.CharField(label="",max_length=150, widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Re-enter the password'}))

    class Meta:
        model = User
        fields = ['new_password1', 'new_password2' ]

class UpdateInfoForm(ModelForm):
    first_name = forms.CharField(label="", max_length=50, required=False, widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Enter first name'})) 
    last_name = forms.CharField(label="", max_length=50, required=False,  widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Enter last name'})) 
    phone = forms.CharField(label="", max_length=50, required=False, widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Enter phone'})) 
    email = forms.EmailField(label="", required=False, widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Enter your E-mail'})) 
    residence = forms.CharField(label="", max_length=50, required=False,  widget= forms.TextInput(attrs={'class': 'form-control', 'placeholder':'Residence'})) 
    
    class Meta:
        model = Customer
        fields = ['first_name', 'last_name', 'phone', 'email','residence']


class Product_form(ModelForm):    
    class Meta:
        model = Product
        fields = "__all__"  
        exclude=['Business']
      
        
class Service_form(ModelForm):
    class Meta:
        model = Service
        fields = "__all__"
        exclude=['Business']
        

class ContactForm(ModelForm):
    first_name =forms.TextInput ()
    last_name = forms.TextInput()
    email = forms.TextInput()
    mobile = forms.TextInput()
    message = forms.TextInput()

    class Meta:
        model = Contact_us
        fields = '__all__' 
        
        
class SearchForm(forms.Form):
    search_term = forms.CharField()
    class Meta:
        fields = ["search_term"]

class ContactForm(ModelForm):
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.CharField()
    mobile = forms.CharField()
    message = forms.TextInput()

    class Meta:
        model = Contact_us
        fields = '__all__' 

    def __init__(self, *args, **kwargs):
        super(ContactForm,self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs['placeholder'] = "First Name"
        self.fields['last_name'].widget.attrs['placeholder'] = "Last Name"
        self.fields['email'].widget.attrs['placeholder'] = "Email"
        self.fields['mobile'].widget.attrs['placeholder'] = "Mobile"
        self.fields['message'].widget.attrs['placeholder'] = "Message"      
 
class OrderForm(forms.ModelForm):   
    class Meta:
        model = Order
        fields = ["address", 'phone', 'email']
        
        
class AppointmentForm(ModelForm):    
    date = forms.DateField(widget=DateTimeInput(attrs={'type': 'date'}))
    time = forms.TimeField(widget=DateTimeInput(attrs={'type': 'time'}))
    phone = forms.CharField(max_length=20)
    
    class Meta:
        model = Appointment
        fields = ['date', 'time', 'phone', "email"]         

from django.db.models.query import QuerySet
from django.shortcuts import render, redirect,get_object_or_404

from django.contrib.auth.forms import UserCreationForm
from .forms import  *
from django.contrib import messages
from django.http import HttpResponseRedirect
from .models import Product, Service, Category,Appointment,Review
from django.contrib.auth import authenticate, login, logout
from django import forms
from django.db.models import Q
from django.contrib.auth.models import User, Group

from django.views.generic import ListView
from .forms import *
from formtools.wizard.views import SessionWizardView
from django.conf import settings
from django.core.files.storage import FileSystemStorage
import os

from django.contrib.auth.decorators import login_required
from .decorators import *

from cart.forms import CartAddProductForm
from cart.cart import Cart

from .tasks import *



# Create your views here.

#Register the Business account
class CreateBusinessUser(SessionWizardView):
    form_list= [CreateUserForm, Business_Form]    
    template_name = 'register.html'
    file_storage = FileSystemStorage(location=os.path.join(settings.MEDIA_ROOT, 'Images'))     

    def done(self, form_list, **kwargs):
        user_form = form_list[0]
        Business_form = form_list[1]
        if user_form.is_valid() and Business_form.is_valid():
            user = user_form.save()              
            Business_form.save(commit=False)
            Business_form.instance.user = user
            Business_form.save()
            group = Group.objects.get(name='Business')
            user.groups.add(group)
            return redirect ('business:login')
        else:
            return render(self.request, self.template_name, {'form_list': form_list})
    
#Register the Customer account
class CreateCustomerUser(SessionWizardView):
    form_list= [CreateUserForm, Customer_Form]    
    template_name = 'register.html'
    
    def done(self, form_list, **kwargs):
        user_form = form_list[0]
        Customer_form = form_list[1]
        if user_form.is_valid() and Customer_form.is_valid():
            user = user_form.save()            
            Customer_form.save(commit=False)
            Customer_form.instance.user = user
            Customer_form.save()
            group = Group.objects.get(name='Customer')
            user.groups.add(group)
            return redirect ('business:login')
        else:
            return render(self.request, self.template_name, {'form_list': form_list})
        
def loginPage(request):
    if request.method == "POST":
        Username = request.POST.get("username")
        Password = request.POST.get("password")
        user = authenticate(request, username=Username, password=Password)
        if user is not None:
            login(request, user)
            return redirect('business:home')
        else:
            messages.info(request, "username or password isn't correct")
    
    context = {}
    return render(request,"login.html", context )

def logoutUser(request):
    logout(request)
    return redirect('business:home')

def profile(request):
    return render(request, 'profile.html',{})


def update_info(request):
    if request.user.is_authenticated:
        current_user = User.objects.get(id=request.user.id)
        form = UpdateInfoForm(request.POST or None, instance= current_user)
        if form.is_valid():
            form.save
            messages.success(request, "Your info has been updated")
            return redirect("business:home")
        return render(request, "update_info.html", {'form': form})

    else:
        messages.success(request, "You must be logged in first")
        return redirect("business:home")


def update_profile(request):
    if request.user.is_authenticated:
        current_user = User.objects.get(id=request.user.id)
        update_form = UpdateProfileForm(request.POST or None, instance= current_user)
        if update_form.is_valid():
            update_form.save
            login(request,current_user)
            messages.success(request, "Your profile has been updated")
            return redirect("business:home")
        

    else:
        messages.success(request, "You must be logged in first")
        return redirect("business:home")
    
    return render(request, "update_profile.html", {'update_form': update_form})
    
def update_password(request):
    if request.user.is_authenticated:
        current_user= request.user
        if request.method == "POST":
            form = UpdatePasswordForm(current_user,request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, "Your password has been updated")
                return redirect('business:login')
            else:
                for error in list(form.errors.values()):
                    messages.error(request, error)
                    return redirect('business:update-password')
        else:
            form = UpdatePasswordForm(current_user)
            return render(request, "update_password.html", {'form': form})
    else:
        messages.success(request, "You must be logged in first")
        return redirect('business:home')
    
def category(request, foo):
    foo = foo.replace('-', ' ')

    try:
        category = Category.objects.get(name=foo)
        product = Product.objects.filter(category=category)
        return render(request, 'category.html', {'products': product, 'category': category})

    except Category.DoesNotExist:
         messages.success(request,"That category does not exist ... Please try again")
         return redirect('business:home')



@login_required
@business 
def add_product(request):
    submitted = False
    form = Product_form()
    
    if request.method == 'POST':
        form = Product_form(request.POST, request.FILES)
        if form.is_valid():
            #auto populates the form with the field business name
            user_id = request.user            
            business_id = Business.objects.get(user=user_id)             
            form.instance.Business = business_id            
            form.save()           
            return redirect('business:manage')
    else:
        form = Product_form()
        if 'submitted' in request.GET:
            submitted= True
    return render(request, 'add_product.html', {'form':form, 'submitted': submitted})

def product(request, pk ):
    product = get_object_or_404(Product, id=pk)
    cart_product_form = CartAddProductForm()

    if request.method =='POST':
       rating=request.POST.get('rating',3)
       content=request.POST.get('content','')
       if content:
            reviews=Review.objects.filter(created_by =request.user,product=product)
            if reviews.count()>0:
                review=reviews.first()
                review.rating=rating
                review.content=content
                review.save()

            else:
             review=Review.objects.create(
                product=product,
                rating=rating,
                content=content,
                created_by=request.user)

            
            return redirect('business:product',pk=pk)
    
    return render(request, 'product.html', {'product': product, 'cart_product_form':cart_product_form})

@login_required
@business 
def add_service(request):
    submitted = False
    form = Service_form()
   
    if request.method == 'POST':
        form = Service_form(request.POST, request.FILES)
        if form.is_valid():
            #auto populates the form with the field business name
            user_id = request.user            
            business_id = Business.objects.get(user=user_id)             
            form.instance.Business = business_id            
            form.save()           
            return redirect('business:manage')
    else:
        form = Service_form
        if 'submitted' in request.GET:
            submitted= True
    return render(request, 'add_service.html', {'form':form, 'submitted': submitted})

def service(request, pk):
    service = get_object_or_404(Service, id=pk)

    if request.method =='POST':
       rating=request.POST.get('rating',2)
       content=request.POST.get('content','')
       if content:
            reviews =Review.objects.filter(created_by =request.user, service=service)
            if review.count()>0:
                reviews=reviews.first()
                reviews.rating=rating
                reviews.content=content
                reviews.save()

            else:
                review = Review.objects.create(
                    service=service,
                    rating=rating,
                    content=content,
                    created_by=request.user)
             
            return redirect('business:service', pk=pk, permanent=True)

    return render(request, 'service.html', {'service': service})

    


def search(request):
    products = None  
    services = None 
   
    if request.method == "POST":
        searched_term = request.POST.get('search-term', '')  
       
        products = Product.objects.filter(product_name__icontains=searched_term)
        services = Service.objects.filter(service_name__icontains=searched_term)
    
        if not products and not services:
            messages.success(request, "That product or service does not exist ... Please try again")
        else:
            print(products)
            print(services)

    context = {
        "products": products,
        "services": services
    }
    return render(request, "search.html", context)

def service(request, pk):
    service = Service.objects.get(id=pk)
    return render(request, 'service.html', {'service': service})

def about(request):
    return render(request,"about.html",{})

def contact(request):
    if request.method == "POST":
        form = ContactForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = ContactForm()
    context = {
        "form":form
    }
    return render(request,"contact.html",context)

def customer(request):
   return render(request, "customer.html")


def dashboard(request):
    products = Product.objects.filter(availability=True).order_by('?')[:4] #filters products that are available with a random order
    services = Service.objects.order_by('?')[:3] 
    discounted = Product.objects.filter(availability=True, discount = True).order_by('?')[:4] 
    return render(request,"home.html",{'products': products, "services":services, 'discounted':discounted})

class ProductListView(ListView):
    model = Product
    template_name = "products.html"
    paginate_by = 6
    context_object_name = 'products'

    def get_queryset(self):        
        queryset = super().get_queryset()   #calls the get_queryset method from the parent
        queryset = queryset.filter(availability=True).order_by('?') #filters the products that are available and orders them randomly
        return queryset

class ServiceListView(ListView):
    model = Service
    template_name = "services.html"
    paginate_by = 3
    context_object_name = 'services'

    def get_queryset(self):
        queryset = super().get_queryset()  
        queryset = queryset.order_by('?') 
        return queryset
 
@login_required
@business       
def ManageBusiness(request):
    if request.user.is_authenticated:
        user = request.user
        #getting the Business associated with the same user
        business = Business.objects.get(user=user)        
        business_id = business.id
        #to filter products that belong that specific business using the business id
        product = Product.objects.filter(Business=business_id)
        service = Service.objects.filter(Business=business_id)
        #to filter just orders, appointment that belong to that specific business
        order = Order.objects.filter(Business=business_id, status=False).order_by('-date')
        appointment = Appointment.objects.filter(Business=business_id).order_by('-created_at')
        
        context = {'products': product,
                   "orders": order,
                   "services":service,
                   "appointments":appointment,
                   }        
        return render(request, 'manage.html', context)
    
def order(request):
    if request.method == "POST":
        form = OrderForm(request.POST)
        if form.is_valid():
            form.save()
    else:
        form = OrderForm()
    context = {
        "form":form
    }
    return render(request,"order.html",context)



def order_create(request):
    cart = Cart(request)
    if request.user.is_authenticated:
        current_user = request.user
        try:
            customer = get_object_or_404(Customer, user= current_user)
        except:
            customer = Create4BusinessCustomer(current_user)
        
        if request.method == "POST":
            form = OrderForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data          
                
                for item in cart:
                    # Create an Order instance for each item in the cart
                    product = get_object_or_404(Product, id=item['product_id'])
                    business = get_object_or_404(Business, id=item['business_id'])
                    
                    Order.objects.create(
                        product = product,
                        Business = business,
                        customer = customer,
                        quantity = item['quantity'],
                        address = cd['address'],
                        phone = cd['phone'],
                        email = cd['email'],                        
                    )
                    alert_mail(business.id)
                cart.clear() 
                last_order = Order.objects.latest('id')                
                Confirmation_mail(last_order.id) 
                messages.success(request, "Your order has been successfully completed.") 
            return redirect('business:home')
        else:
            form = OrderForm()    
            return render(request,'order/create.html', {'form':form, 'cart':cart})
    else:
        return redirect("business:login")


def appointment(request, service_id, business_id):
    business = get_object_or_404(Business, id=business_id)
    service = get_object_or_404(Service, id=service_id)
    if request.user.is_authenticated:
        current_user = request.user
        try:
            customer = get_object_or_404(Customer, user= current_user)
        except:
            customer = Create4BusinessCustomer(current_user)

        if request.method == "POST":
            form = AppointmentForm(request.POST)
            if form.is_valid():
                cd = form.cleaned_data
                Appointment.objects.create(
                    Business = business,
                    customer = customer,
                    service = service,
                    date = cd['date'],
                    time = cd['time'],
                    phone = cd['phone'],
                    email = cd['email']
                )
            alert_mail(business.id)
            messages.success(request, "You have booked an Appointment.") 
            last_appointment = Appointment.objects.latest('id')                
            Appointment_mail(last_appointment.id) 
            return redirect("business:home")
        else:
            form = AppointmentForm()        
            return render(request,"appointment.html",{"form":form})
    else:
        return redirect("business:login")

def BusinessPage(request, Business_id):
    business = get_object_or_404(Business, id=Business_id)
    product = Product.objects.filter(Business=Business_id)
    service = Service.objects.filter(Business=Business_id)
    return render(request, 'businesspage.html', {'business':business, 'products':product, 'services':service})


def check(request, pk):
    check_order = get_object_or_404(Order, id = pk)
    check_order.status = True
    check_order.save()
    return redirect('business:manage')

def approve(request, pk):
    appointment = get_object_or_404(Appointment, id = pk)
    appointment.status = True
    appointment.save()
    Approve_appointment_mail(pk)
    return redirect('business:manage')

def UpdateProduct(request, pdt_id):
    record = get_object_or_404(Product, id=pdt_id)
    form = Product_form(initial={
        'product_name':record.product_name,
        'price': record.price,
        'Description': record.Description,
        'availability': record.availability,
        'photo': record.photo,
        'category': record.category,
        'discount': record.discount,
        'discounted_price': record.discounted_price
    })

    if request.method == 'POST':
        form = Product_form(request.POST, instance=record)
        if form.is_valid():
            user_id = request.user
            business_id = Business.objects.get(user=user_id)
            form.instance.Business = business_id
            form.save()
            return redirect( 'business:manage')
        else:            
            return render(request, 'update.html', {'form': form, 'error': 'Invalid form data'})

    return render(request, 'update.html', {'form': form})

def UpdateService(request, service_id):
    service_record = get_object_or_404(Service, id=service_id)
    form = Service_form(initial={
        'service_name':service_record.service_name,
        'price': service_record.price,
        'description': service_record.description,        
        'photo': service_record.photo,
        'category': service_record.category,
        'opening_time': service_record.opening_time,
        'closing_time': service_record.closing_time,
        'location': service_record.location,
    })

    if request.method == 'POST':
        form = Service_form(request.POST, instance=service_record)
        if form.is_valid():
            user_id = request.user
            business_id = Business.objects.get(user=user_id)
            form.instance.Business = business_id
            form.save()
            return redirect( 'business:manage')
        else:            
            return render(request, 'update.html', {'form': form, 'error': 'Invalid form data'})

    return render(request, 'update.html', {'form': form})


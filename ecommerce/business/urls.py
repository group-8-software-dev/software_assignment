from django.urls import path
from . import views

from .views import *
app_name = 'business'
urlpatterns = [
    
    path("", views.dashboard, name="home"), 
    path('login/', views.loginPage, name='login'),
    path('logout/', views.logoutUser , name="logout" ),
    path('add_product/', views.add_product, name= 'add-product'),
    path('add_service/', views.add_service, name= 'add-service'),    
    path('profile/', views.profile, name= 'profile'),
    path('update_info/', views.update_info, name= 'update-info'),
    path('update_profile/', views.update_profile, name= 'update-profile'),
    path('update_password/', views.update_password, name= 'update-password'),
    path('product/<int:pk>', views.product, name= 'product'),
    path('service/<int:pk>', views.service, name= 'service'),
    path('category/<str:foo>', views.category, name= 'category'),
    path('search/', views.search, name= 'search'),
    path('about/',views.about,name='about'),
    path("contact/",views.contact,name="contact"),
    path('customer/', views.customer,name='customer'),    
    path("services/", ServiceListView.as_view(), name="service-list"),    
    path("products/", ProductListView.as_view(), name="product-list"),
    path('registerBusiness/',CreateBusinessUser.as_view(), name='register_business'),
    path('registerCustomer/',CreateCustomerUser.as_view(), name='register_customer'),    
    path("manage/",views.ManageBusiness, name ='manage'),
    path("order/",views.order,name='order'),
    path("create/",views.order_create,name='order_create'),
    path('business/<int:Business_id>/', views.BusinessPage, name='business_Page'),
    path("check/<int:pk>/", views.check, name="check"),
    path("approve/<int:pk>/", views.approve, name="approve"),
    path("updatePdt/<int:pdt_id>/", views.UpdateProduct, name="UpdateProduct"),
    path("updateService/<int:service_id>/", views.UpdateService, name="UpdateService"),
    path("appointment/<int:service_id>/<int:business_id>", views.appointment, name="book"),
    
]

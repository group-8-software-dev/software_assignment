from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib import messages


def business(view_func):
    """Decorator that restricts access to views to users in the 'Business' group."""
    def wrapper(request, *args, **kwargs):
        if request.user.groups.filter(name='Business').exists():
            return view_func(request, *args, **kwargs)
        else:
            messages.info(request, " Customer Account: Does not have permission to access this ")
            return redirect("business:home")
    return wrapper


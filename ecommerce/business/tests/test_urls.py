from django.test import TestCase, SimpleTestCase
from business.urls import *
from django.urls import resolve, reverse
from business.views import registerPage



from django.urls import reverse
from django.test import Client

class TestRegisterPage(SimpleTestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('register')  
        response = client.get(url)
        assert response.status_code == 200

class TestLoginPage(SimpleTestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('login')  
        response = client.get(url)
        assert response.status_code == 200

class TestLogoutPage(SimpleTestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('logout')  
        response = client.get(url)
        self.assertEqual(response.status_code, 302)

class TestAddProductPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('add-product')  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestAddServicePage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('add-service')  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestProfilePage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('profile')  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestAddProductPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('add-product')  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestUpdateInfoPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('update-info')  
        response = client.get(url)
        self.assertEqual(response.status_code, 302)

class TestUpdateProfilePage(SimpleTestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('update-profile')  
        response = client.get(url)
        self.assertEqual(response.status_code, 302)

class TestUpdatePasswordPage(SimpleTestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('update-password')  
        response = client.get(url)
        self.assertEqual(response.status_code, 302)

# class TestProductPage(TestCase):

#     def test_register_page_status_code(self):
#         client = Client()
#         url = reverse('product', args=[1])  
#         response = client.get(url)
#         self.assertEqual(response.status_code, 200)
        
# class TestServicePage(TestCase):

#     def test_register_page_status_code(self):
#         client = Client()
#         url = reverse('service', args=[1])  
#         response = client.get(url)
#         self.assertEqual(response.status_code, 200)
        
class TestCategoryPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('category', args=[1])  
        response = client.get(url)
        self.assertEqual(response.status_code, 302)

class TestSearchPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('search')  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestAboutPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('about',)  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestContactPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('contact',)  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestCustomerPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('customer',)  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestServiceListViewPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('service-list',)  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestProductListViewPage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('product-list',)  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestManagePage(TestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('manage')  
        response = client.get(url)
        self.assertEqual(response.status_code, 200)

class TestOrderPage(SimpleTestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('order')  
        response = client.get(url)
        self.assertEqual(response.status_code, 302)

class TestAppointmentPage(SimpleTestCase):

    def test_register_page_status_code(self):
        client = Client()
        url = reverse('book')  
        response = client.get(url)
        self.assertEqual(response.status_code, 302)



        


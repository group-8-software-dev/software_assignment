

from django.test import TestCase, Client
from django.urls import reverse
from business.models import User
from django.contrib.auth import get_user_model 
from business.forms import *
from cart.views import*
from django.contrib.messages import get_messages
# class TestRegisterPage(TestCase):
#     def test_registration_successful(self):
#         # Create a test user
#         user_data = {
#             'username': 'username',
#             'email': 'email',
#             # 'password1': 'password1',
#             # 'password2': 'password2',
#         }
#         User.objects.create_user(**user_data)

#         # Simulate a POST request with valid form data
#         response = self.client.post(reverse('register'), data=user_data)

#         # Check if the user was redirected to the login page
#         self.assertRedirects(response, reverse('login'))

#         # Optionally, check if the user was actually saved in the database
#         self.assertTrue(User.objects.filter(username='username').exists())


class LoginPageViewTest(TestCase):
    def setUp(self):
        # Create a test user
        self.username = "testuser"
        self.password = "testpassword"
        self.user = User.objects.create_user(username=self.username, password=self.password)
        self.client = Client()

    def test_login_successful(self):
        # Simulate a POST request with valid credentials
        response = self.client.post(reverse("login"), {"username": self.username, "password": self.password})
        self.assertEqual(response.status_code, 302)  # Expect a redirect (status code 302)
        self.assertRedirects(response, reverse("home"))  # Expect redirection to the 'home' page

    def test_login_invalid_credentials(self):
        # Simulate a POST request with invalid credentials
        response = self.client.post(reverse("login"), {"username": "invaliduser", "password": "wrongpassword"})
        self.assertEqual(response.status_code, 200)  # Expect a successful response (status code 200)
        expected_message = "username or password isn't correct"
        self.assertContains(response, expected_message, html=True)

    def test_login_page_render(self):
        # Simulate a GET request to render the login page
        response = self.client.get(reverse("login"))
        self.assertEqual(response.status_code, 200)  # Expect a successful response (status code 200)
        self.assertTemplateUsed(response, "login.html")  # Expect the correct template to be used

class TestLogoutPage(TestCase):
    def setUp(self):
        self.client = Client()
        #self.user = get_user_model().objects.create_user(username='testuser', email='test@example.com', password='testpassword')
        

    def test_logout_successful(self):
        # Create a test user (you can adjust the username, email, and password)
        User = get_user_model()
        user = User.objects.create_user(username='testuser', email='test@example.com', password='testpassword')

        # Log in the user (optional)
        self.client.login(username='testuser', password='testpassword')

        # Check if the user is authenticated
        self.assertIn('_auth_user_id', self.client.session)
        response = self.client.get(reverse('logout'))
        self.assertRedirects(response, reverse('login'))


    def test_logout_page_render(self):
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, 302)

class TestUpdateInfoPage(TestCase):
    def setUp(self):
        self.client = Client()

    def test_user_is_authenticated(self):
        # Create a test user (you can adjust the username, email, and password)
        User = get_user_model()
        user = User.objects.create_user(username='testuser', email='test@example.com', password='testpassword')

        # Log in the user (optional)
        self.client.login(username='testuser', password='testpassword')

        # Check if the user is authenticated
        self.assertIn('_auth_user_id', self.client.session)
        response = self.client.get(reverse('update-info'))
        self.assertTemplateUsed(response, "update_info.html")

    def test_update_Info_page_render(self):
        response = self.client.get(reverse('update-info'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))

class TestUpdateProfilePage(TestCase):
    def setUp(self):
        self.client = Client()

    def test_user_is_authenticated(self):
        # Create a test user (you can adjust the username, email, and password)
        User = get_user_model()
        user = User.objects.create_user(username='testuser', email='test@example.com', password='testpassword')

        # Log in the user (optional)
        self.client.login(username='testuser', password='testpassword')

        # Check if the user is authenticated
        self.assertIn('_auth_user_id', self.client.session)
        response = self.client.post(reverse('update-profile'))
        self.assertEqual(response.status_code, 200)

    def test_update_Info_page_render(self):
        response = self.client.get(reverse('update-info'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))

class TestUpdatePasswordPage(TestCase):
    def setUp(self):
        self.client = Client()
        
    def test_user_is_authenticated(self):
        # Create a test user (you can adjust the username, email, and password)
        User = get_user_model()
        user = User.objects.create_user(username='testuser', email='test@example.com', password='testpassword')

        # Log in the user (optional)
        self.client.login(username='testuser', password='testpassword')

        # Check if the user is authenticated
        self.assertIn('_auth_user_id', self.client.session)
        response = self.client.post(reverse('update-password'), {'new_password1': 'newpass123', 'new_password2': 'newpass123'})
        #self.assertTemplateUsed(response, "update_password.html")
        self.assertEqual(response.status_code, 302)

    def test_update_password_page_render(self):
        response = self.client.get(reverse('update-password'))
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, reverse('home'))

class TestCategoryPage(TestCase):
    def setUp(self):
        self.client = Client()
        # Create a test category (you can adjust the name)
        self.category = Category.objects.create(name='Test Category')

    def test_category_exists(self):
        # Simulate a request for an existing category
        response = self.client.post(reverse('category', args=['test-category']))

        self.assertEqual(response.status_code, 302)

    def test_category_does_not_exist(self):
        # Simulate a request for a non-existing category
        response = self.client.get(reverse('category', args=['non-existent-category']))

        # Check if the user is redirected to the home page
        self.assertRedirects(response, reverse('home'))


class TestAddProductPage(TestCase):
    def setUp(self):
        self.client = Client()

    def test_add_product_form_submission(self):
        # Simulate a POST request with valid form data
        response = self.client.post(reverse('add-product'), {'field1': 'value1', 'field2': 'value2'})

        # Check if the product was added successfully
        self.assertEqual(response.status_code, 200)  # Expect a redirect (status code 302)
        # You can also check other conditions, such as verifying the saved product data

    def test_add_product_form_display(self):
        # Simulate a GET request to display the form
        response = self.client.get(reverse('add-product'))

        # Check if the form is displayed correctly
        self.assertEqual(response.status_code, 200)  # Expect a successful response (status code 200)
        self.assertTemplateUsed(response, 'add_product.html')  # Expect the correct template to be used

class TestAddServicePage(TestCase):
    def setUp(self):
        self.client = Client()

    def test_add_service_form_submission(self):
        # Simulate a POST request with valid form data
        response = self.client.post(reverse('add-service'), {'field1': 'value1', 'field2': 'value2'})

        # Check if the product was added successfully
        self.assertEqual(response.status_code, 200)  # Expect a redirect (status code 302)
        # You can also check other conditions, such as verifying the saved product data

    def test_add_product_form_display(self):
        # Simulate a GET request to display the form
        response = self.client.get(reverse('add-service'))

        # Check if the form is displayed correctly
        self.assertEqual(response.status_code, 200)  # Expect a successful response (status code 200)
        self.assertTemplateUsed(response, 'add_service.html')  # Expect the correct template to be used



class ServiceViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.service = Service.objects.create(name='Test Service')
        self.review = Review.objects.create(service=self.service, created_by=self.user, rating=3, content='Test review content')

    def test_post_review(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:service', kwargs={'pk': self.service.pk}), {
            'rating': 4,
            'content': 'New test review content'
        })
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.review.refresh_from_db()
        self.assertEqual(self.review.rating, 4)
        self.assertEqual(self.review.content, 'New test review content')

    def test_post_empty_content_review(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:service', kwargs={'pk': self.service.pk}), {
            'rating': 4,
            'content': ''
        })
        self.assertEqual(response.status_code, 200)  # Expecting to stay on the same page
        self.review.refresh_from_db()
        self.assertEqual(self.review.rating, 3)  # Rating should remain unchanged
        self.assertEqual(self.review.content, 'Test review content')  # Content should remain unchanged

    def test_get_service_page(self):
        response = self.client.get(reverse('business:service', kwargs={'pk': self.service.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'service.html')
        self.assertEqual(response.context['service'], self.service)

class OrderCreateViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.cart = Cart.objects.create(user=self.user)
        self.customer = Customer.objects.create(user=self.user)
        self.product = Product.objects.create(name='Test Product')
        self.business = Business.objects.create(name='Test Business')

    def test_order_create_authenticated_user(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:order_create'), {
            'address': 'Test Address',
            'phone': '1234567890',
            'email': 'test@example.com',
            # Add other required fields here
        })
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.assertEqual(Order.objects.count(), 1)
        self.assertEqual(Order.objects.first().customer, self.customer)
        self.assertEqual(Order.objects.first().product, self.product)
        self.assertEqual(Order.objects.first().Business, self.business)
        self.assertEqual(Cart.objects.count(), 0)  # Expecting the cart to be cleared
        self.assertEqual(len(get_messages(response)), 1)  # Expecting a success message

    def test_order_create_unauthenticated_user(self):
        response = self.client.post(reverse('business:order_create'))
        self.assertEqual(response.status_code, 302)  # Expecting a redirect to login page

    def test_order_create_invalid_form(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:order_create'), {})
        self.assertEqual(response.status_code, 200)  # Expecting to stay on the same page
        self.assertEqual(Order.objects.count(), 0)  # Expecting no order to be created



class AppointmentViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.business = Business.objects.create(name='Test Business')
        self.service = Service.objects.create(name='Test Service')

    def test_appointment_authenticated_user(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:appointment', kwargs={'service_id': self.service.id, 'business_id': self.business.id}), {
            'date': '2024-05-10',  # Example date
            'time': '10:00',  # Example time
            'phone': '1234567890',
            'email': 'test@example.com',
            # Add other required fields here
        })
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.assertEqual(Appointment.objects.count(), 1)
        self.assertEqual(Appointment.objects.first().customer.user, self.user)
        self.assertEqual(Appointment.objects.first().service, self.service)
        self.assertEqual(Appointment.objects.first().Business, self.business)
        self.assertEqual(len(get_messages(response)), 1)  # Expecting a success message

    def test_appointment_unauthenticated_user(self):
        response = self.client.post(reverse('business:appointment', kwargs={'service_id': self.service.id, 'business_id': self.business.id}))
        self.assertEqual(response.status_code, 302)  # Expecting a redirect to login page

    def test_appointment_invalid_form(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:appointment', kwargs={'service_id': self.service.id, 'business_id': self.business.id}), {})
        self.assertEqual(response.status_code, 200)  # Expecting to stay on the same page
        self.assertEqual(Appointment.objects.count(), 0)  # Expecting no appointment to be created

   

class ProductViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.product = Product.objects.create(name='Test Product')

    def test_product_get(self):
        response = self.client.get(reverse('business:product', kwargs={'pk': self.product.pk}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'product.html')
        self.assertEqual(response.context['product'], self.product)
        self.assertIsInstance(response.context['cart_product_form'], CartAddProductForm)

    def test_product_post_review(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:product', kwargs={'pk': self.product.pk}), {
            'rating': 4,
            'content': 'Test review content',
        })
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.assertEqual(Review.objects.count(), 1)
        self.assertEqual(Review.objects.first().created_by, self.user)
        self.assertEqual(Review.objects.first().product, self.product)
        self.assertEqual(len(get_messages(response)), 0)  # No messages expected after successful review

    def test_product_post_empty_content_review(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:product', kwargs={'pk': self.product.pk}), {
            'rating': 4,
            'content': '',
        })
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.assertEqual(Review.objects.count(), 0)  # No review should be created if content is empty
        self.assertEqual(len(get_messages(response)), 0)  # No messages expected

    def test_product_unauthenticated_user(self):
        response = self.client.post(reverse('business:product', kwargs={'pk': self.product.pk}))
        self.assertEqual(response.status_code, 302)  # Expecting a redirect to login page

class OrderViewTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_order_get(self):
        response = self.client.get(reverse('business:order'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'order.html')
        self.assertIsInstance(response.context['form'], OrderForm)

    def test_order_post(self):
        form_data = {
            'address': 'Test Address',
            'phone': '1234567890',
            'email': 'test@example.com',
            # Add other required fields here
        }
        response = self.client.post(reverse('business:order'), form_data)
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.assertEqual(Order.objects.count(), 1)
        # Add assertions to verify the saved order data if needed

    def test_order_invalid_form(self):
        response = self.client.post(reverse('business:order'), {})
        self.assertEqual(response.status_code, 200)  # Expecting to stay on the same page
        self.assertEqual(Order.objects.count(), 0)  # Expecting no order to be created

 

class ManageBusinessViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.business = Business.objects.create(user=self.user, name='Test Business')

    def test_manage_business_authenticated_user(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('business:manage_business'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'manage.html')
        self.assertIn('products', response.context)
        self.assertIn('orders', response.context)
        self.assertIn('services', response.context)
        self.assertIn('appointments', response.context)
        # Add more assertions as needed to verify the context data

    def test_manage_business_unauthenticated_user(self):
        response = self.client.get(reverse('business:manage_business'))
        self.assertEqual(response.status_code, 302)  # Expecting a redirect to login page

class CheckViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.order = Order.objects.create(status=False)

    def test_check_post(self):
        response = self.client.post(reverse('business:check', kwargs={'pk': self.order.pk}))
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.order.refresh_from_db()
        self.assertTrue(self.order.status)  # Expecting the order status to be True after the view is called

    def test_check_invalid_order_id(self):
        response = self.client.post(reverse('business:check', kwargs={'pk': 999}))
        self.assertEqual(response.status_code, 404)  # Expecting a 404 not found response

class ApproveViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.appointment = Appointment.objects.create(status=False)

    def test_approve_post(self):
        response = self.client.post(reverse('business:approve', kwargs={'pk': self.appointment.pk}))
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.appointment.refresh_from_db()
        self.assertTrue(self.appointment.status)  # Expecting the appointment status to be True after the view is called

    def test_approve_invalid_appointment_id(self):
        response = self.client.post(reverse('business:approve', kwargs={'pk': 999}))
        self.assertEqual(response.status_code, 404)  # Expecting a 404 not found response

   

class ContactViewTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_contact_get(self):
        response = self.client.get(reverse('business:contact'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'contact.html')
        self.assertIsInstance(response.context['form'], ContactForm)

    def test_contact_post_valid_form(self):
        form_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'test@example.com',
            'mobile': '1234567890',
            'message': 'Test message',
        }
        response = self.client.post(reverse('business:contact'), form_data)
        self.assertEqual(response.status_code, 302)  # Expecting a redirect


    def test_contact_post_invalid_form(self):
        response = self.client.post(reverse('business:contact'), {})
        self.assertEqual(response.status_code, 200)  # Expecting to stay on the same page
        self.assertContains(response, 'This field is required.')  # Expecting form validation errors


class BusinessPageViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.business = Business.objects.create(name='Test Business')
        self.product = Product.objects.create(name='Test Product', Business=self.business)
        self.service = Service.objects.create(name='Test Service', Business=self.business)

    def test_business_page_get(self):
        response = self.client.get(reverse('business:business_page', kwargs={'Business_id': self.business.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'businesspage.html')
        self.assertEqual(response.context['business'], self.business)
        self.assertQuerysetEqual(response.context['products'], [repr(self.product)])
        self.assertQuerysetEqual(response.context['services'], [repr(self.service)])

    def test_nonexistent_business_page(self):
        response = self.client.get(reverse('business:business_page', kwargs={'Business_id': 999}))
        self.assertEqual(response.status_code, 404)  # Expecting a 404 not found response



class UpdateServiceViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.business = Business.objects.create(user=self.user)
        self.service = Service.objects.create(
            service_name='Test Service',
            price='updated price',
            description='Test description',
            photo='test_photo.jpg',
            category='Test category',
            opening_time='08:00',
            closing_time='17:00',
            location='Test location',
            Business=self.business
        )

    def test_update_service_get(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('business:update_service', kwargs={'service_id': self.service.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'update.html')
        self.assertIsInstance(response.context['form'], Service_form)

    def test_update_service_post_valid_form(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:update_service', kwargs={'service_id': self.service.id}), {
            'service_name': 'Updated Service Name',
            'price': 'updated price',
            'description': 'Updated description',
            'photo': 'updated_photo.jpg',
            'category': 'Updated category',
            'opening_time': '09:00',
            'closing_time': '18:00',
            'location': 'Updated location',
            
        })
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.service.refresh_from_db()
        self.assertEqual(self.service.service_name, 'Updated Service Name')
        self.assertEqual(self.service.price, 20.0)
        self.assertEqual(self.service.description, 'Updated description')
      

    def test_update_service_post_invalid_form(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:update_service', kwargs={'service_id': self.service.id}), {})
        self.assertEqual(response.status_code, 200)  # Expecting to stay on the same page
        self.assertContains(response, 'Invalid form data')  # Expecting an error message

class UpdateProductViewTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='testuser', password='testpassword')
        self.business = Business.objects.create(user=self.user)
        self.product = Product.objects.create(
            product_name='Test Product',
            price='required amount',
            Description='Test description',
            availability=True,
            photo='test_photo.jpg',
            category='Test category',
            discount='discount',
            discounted_price='amount',
            Business=self.business
        )

    def test_update_product_get(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.get(reverse('business:update_product', kwargs={'pdt_id': self.product.id}))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'update.html')
        self.assertIsInstance(response.context['form'], Product_form)

    def test_update_product_post_valid_form(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:update_product', kwargs={'pdt_id': self.product.id}), {
            'product_name': 'Updated Product Name',
            'price': 20.0,
            'Description': 'Updated description',
            'availability': False,
            'photo': 'updated_photo.jpg',
            'category': 'Updated category',
            'discount': 'required discount',
            'discounted_price': 'required discount price',
            
        })
        self.assertEqual(response.status_code, 302)  # Expecting a redirect
        self.product.refresh_from_db()
        self.assertEqual(self.product.product_name, 'Updated Product Name')
        self.assertEqual(self.product.price, 20.0)
        self.assertEqual(self.product.Description, 'Updated description')
        

    def test_update_product_post_invalid_form(self):
        self.client.login(username='testuser', password='testpassword')
        response = self.client.post(reverse('business:update_product', kwargs={'pdt_id': self.product.id}), {})
        self.assertEqual(response.status_code, 200)  # Expecting to stay on the same page
        self.assertContains(response, 'Invalid form data')  # Expecting an error message

  








# class SearchViewTest(TestCase):
#     def setUp(self):
#         self.client = Client()
#         # Create a test product (you can adjust the name and description)
#         self.product = Product.objects.create(product_name='Test Product', Description='Test description')

#     def test_search_form_submission(self):
#         # Simulate a POST request with valid form data
#         response = self.client.post(reverse('search'), {'searched': 'Test Product'})

#         # Check if the product search results are displayed
#         self.assertEqual(response.status_code, 200)  # Expect a successful response (status code 200)
#         # You can also check other conditions, such as verifying the displayed search results

#     def test_search_form_display(self):
#         # Simulate a GET request to display the form
#         response = self.client.get(reverse('search'))

#         # Check if the form is displayed correctly
#         self.assertEqual(response.status_code, 200)  # Expect a successful response (status code 200)
#         self.assertTemplateUsed(response, 'search.html')  # Expect the correct template to be used












        
        


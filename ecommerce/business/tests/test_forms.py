from django.contrib.auth.models import User
from django.test import  TestCase
from business.forms import *

class CreateUserFormTest(TestCase):
    def test_valid_form(self):
        form_data = {
            'first_name': 'Test',
            'last_name': 'User',
            'username': 'testuser',
            'email': 'testuser@example.com',
            'password1': 'testpassword123',
            'password2': 'testpassword123',
        }
        form = CreateUserForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form_data = {
            'first_name': 'Test',
            'last_name': 'User',
            'username': '',  # Empty username should fail validation
            'email': 'invalidemail',  # Invalid email format should fail validation
            'password1': 'password',  # Weak password should fail validation
            'password2': 'password123',  # Password mismatch should fail validation
        }
        form = CreateUserForm(data=form_data)
        self.assertFalse(form.is_valid())


class BusinessFormTest(TestCase):
    def test_business_form_valid_data(self):
        # Create a dictionary containing valid data for the form
        form_data = {
            'name': 'Test Business',
            'description': 'This is a test business.',
            # Include other required fields...
        }

        # Create a form instance with the provided data
        form = Business_Form(data=form_data)

        # Check if the form is valid
        self.assertFalse(form.is_valid())

    def test_business_form_invalid_data(self):
        # Create a dictionary containing invalid data for the form
        form_data = {
            # Omitting required fields intentionally to make the form invalid
        }

        # Create a form instance with the provided data
        form = Business_Form(data=form_data)

        # Check if the form is not valid
        self.assertFalse(form.is_valid())


class CustomerFormTest(TestCase):
    def test_customer_form_valid_data(self):
        # Create a dictionary containing valid data for the form
        form_data = {
            'name': 'Test Customer',
            'email': 'test@example.com',
            # Include other required fields...
        }

        # Create a form instance with the provided data
        form = Customer_Form(data=form_data)

        # Check if the form is valid
        self.assertFalse(form.is_valid())

    def test_customer_form_invalid_data(self):
        # Create a dictionary containing invalid data for the form
        form_data = {
            # Omitting required fields intentionally to make the form invalid
        }

        # Create a form instance with the provided data
        form = Customer_Form(data=form_data)

        # Check if the form is not valid
        self.assertFalse(form.is_valid())


class UpdateProfileFormTest(TestCase):
    def test_update_profile_form_valid_data(self):
        # Create a user instance for testing
        user = User.objects.create_user(username='testuser', email='test@example.com', password='testpassword')
        
        # Create a dictionary containing valid data for the form
        form_data = {
            'email': 'updated@example.com',
            'first_name': 'Updated First Name',
            'last_name': 'Updated Last Name',
        }

        # Create a form instance with the provided data
        form = UpdateProfileForm(data=form_data, instance=user)

        # Check if the form is valid
        self.assertTrue(form.is_valid())

    def test_update_profile_form_invalid_data(self):
        # Create a user instance for testing
        user = User.objects.create_user(username='testuser', email='test@example.com', password='testpassword')

        # Create a dictionary containing invalid data for the form
        form_data = {
            # Omitting required fields intentionally to make the form invalid
        }

        # Create a form instance with the provided data
        form = UpdateProfileForm(data=form_data, instance=user)

        # Check if the form is not valid
        self.assertFalse(form.is_valid())

class UpdatePasswordFormTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username='testuser', password='old_password')
        self.client.login(username='testuser', password='old_password')

    def test_valid_update_password_form(self):
        # Example valid form data
        form_data = {
            'new_password1': 'new_password',
            'new_password2': 'new_password',
        }
        form = UpdatePasswordForm(user=self.user, data=form_data)
        self.assertTrue(form.is_valid())

    def test_empty_fields(self):
        # Test with empty form data
        form_data = {
            'new_password1': '',
            'new_password2': '',
        }
        form = UpdatePasswordForm(user=self.user, data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 2)  # Expecting errors for both fields being empty

    def test_password_mismatch(self):
        # Test with mismatching passwords
        form_data = {
            'new_password1': 'new_password_1',
            'new_password2': 'new_password_2',
        }
        form = UpdatePasswordForm(user=self.user, data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('new_password2', form.errors)  # Expecting error for password mismatch




class UpdateInfoFormTest(TestCase):
    def test_update_info_form_valid_data(self):
        # Create a dictionary containing valid data for the form
        form_data = {
            'first_name': 'ValidFirst',
            'last_name': 'ValidLast',
            'phone': '1234567890',
            'email': 'valid@example.com',
            'residence': 'Valid Residence',
        }

        # Create a form instance with the provided data
        form = UpdateInfoForm(data=form_data)

        # Check if the form is valid
        self.assertTrue(form.is_valid())

    def test_update_info_form_invalid_data(self):
        # Create a dictionary containing invalid data for the form
        form_data = {
            # Omitting required fields intentionally to make the form invalid
        }

        # Create a form instance with the provided data
        form = UpdateInfoForm(data=form_data)

        # Check if the form is not valid
        self.assertTrue(form.is_valid())


class ProductFormTest(TestCase):
    def test_product_form_valid_data(self):
        # Create a dictionary containing valid data for the form
        form_data = {
            'name': 'Test Product',
            'description': 'This is a test product.',
            # Include other required fields...
        }

        # Create a form instance with the provided data
        form = Product_form(data=form_data)

        # Check if the form is valid
        self.assertFalse(form.is_valid())

    def test_product_form_invalid_data(self):
        # Create a dictionary containing invalid data for the form
        form_data = {
            # Omitting required fields intentionally to make the form invalid
        }

        # Create a form instance with the provided data
        form = Product_form(data=form_data)

        # Check if the form is not valid
        self.assertFalse(form.is_valid())

    def test_empty_fields(self):
        # Test with empty form data
        form_data = {}
        form = Product_form(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)  # Expecting an error for the missing 'name' field   

class SearchFormTest(TestCase):
    def test_valid_search_form(self):
        # Example valid form data
        form_data = {
            'search_term': 'example search term',
        }
        form = SearchForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_empty_search_term(self):
        # Test with an empty search term
        form_data = {
            'search_term': '',
        }
        form = SearchForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)  # Expecting an error for the missing search term
     
     
     
class ServiceFormTest(TestCase):
    def test_service_form_valid_data(self):
        # Create a dictionary containing valid data for the form
        form_data = {
            'name': 'Test Service',
            'description': 'This is a test service.',
            # Include other required fields...
        }

        # Create a form instance with the provided data
        form = Service_form(data=form_data)

        # Check if the form is valid
        self.assertFalse(form.is_valid())

    def test_service_form_invalid_data(self):
        # Create a dictionary containing invalid data for the form
        form_data = {
            # Omitting required fields intentionally to make the form invalid
        }

        # Create a form instance with the provided data
        form = Service_form(data=form_data)

        # Check if the form is not valid
        self.assertFalse(form.is_valid())
        
    def test_empty_fields(self):
        # Test with empty form data
        form_data = {}
        form = Service_form(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)  # Expecting an error for the missing 'name' field   

class ContactFormTest(TestCase):
    def test_valid_contact_form(self):
        form_data = {
            'first_name': 'Joselyn',
            'last_name': 'Nantongo',
            'email': 'joselyn@example.com',
            'mobile': '1234567890',
            'message': 'This is a test message.'
        }
        form = ContactForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_empty_fields(self):
        form_data = {
            'first_name': '',
            'last_name': '',
            'email': '',
            'mobile': '',
            'message': ''
        }
        form = ContactForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 5)  # Expecting errors for all fields being empty

    def test_invalid_email_format(self):
        form_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'invalid_email',  # Invalid email format
            'mobile': '1234567890',
            'message': 'This is a test message.'
        }
        form = ContactForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('email', form.errors)  # Expecting error for invalid email format

    def test_message_character_limit(self):
        # Creating a message with more than 1000 characters
        message = 'a' * 1001
        form_data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'john@example.com',
            'mobile': '1234567890',
            'message': message
        }
        form = ContactForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('message', form.errors)  # Expecting error for exceeding character limit


class OrderFormTest(TestCase):
    def test_valid_order_form(self):
        form_data = {
            'address': '123 Main St',
            'phone': '1234567890',
            'email': 'test@example.com'
        }
        form = OrderForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_empty_fields(self):
        form_data = {
            'address': '',
            'phone': '',
            'email': ''
        }
        form = OrderForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 3)  # Expecting errors for all fields being empty

    def test_invalid_email_format(self):
        form_data = {
            'address': '123 Main St',
            'phone': '1234567890',
            'email': 'invalid_email'  # Invalid email format
        }
        form = OrderForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('email', form.errors)  # Expecting error for invalid email format



class AppointmentFormTest(TestCase):
    def test_valid_appointment_form(self):
        form_data = {
            'date': '2024-05-10',  # Example date
            'time': '10:00',  # Example time
            'phone': '1234567890',
            'email': 'test@example.com'
        }
        form = AppointmentForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_empty_fields(self):
        form_data = {
            'date': '',
            'time': '',
            'phone': '',
            'email': ''
        }
        form = AppointmentForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 4)  # Expecting errors for all fields being empty

    def test_invalid_phone_format(self):
        form_data = {
            'date': '2024-05-10',
            'time': '10:00',
            'phone': 'invalid_phone',  # Invalid phone format
            'email': 'test@example.com'
        }
        form = AppointmentForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertIn('phone', form.errors)  # Expecting error for invalid phone format

from django.forms import ValidationError
from django.test import TestCase
from django.contrib.auth.models import User
from business.models import Business,Product,Category,Service_category,Service,Customer, Profile,Order,Contact_us, Appointment,Review 
from django.utils import timezone
from django.db import models
from django.core.files.uploadedfile import SimpleUploadedFile
from datetime import date, time
from datetime import datetime
from django.db import IntegrityError

from ecommerce.business.views import product

class TestProfile(models.Model):
    # ... other fields ...
    date_created = models.DateTimeField(default=timezone.now)

class BusinessModelTest(TestCase):
    def setUp(self):
        # Create a user for the OneToOneField relationship
        self.user = User.objects.create_user(username='testuser', password='12345')

        # Create a Business instance
        self.business = Business.objects.create(
            user=self.user,
            name='Test Business',
            business_contact='1234567890',
            email='test@example.com',
            location='Test Location',
            description='Test Description',
            # Assuming 'logos/logo.png' is a valid path in your MEDIA_ROOT
            logo='logos/logo.png'
        )

    def test_business_creation(self):
        # Check that the Business instance has been correctly created
        self.assertEqual(self.business.name, 'Test Business')
        self.assertEqual(self.business.business_contact, '1234567890')
        self.assertEqual(self.business.email, 'test@example.com')
        self.assertEqual(self.business.location, 'Test Location')
        self.assertEqual(self.business.description, 'Test Description')
        self.assertTrue(self.business.logo, 'logos/logo.png')

        # Check __str__ method
        self.assertEqual(str(self.business), 'Test Business')



class ProductModelTest(TestCase):
    def setUp(self):
        # Create sample data for testing
        business = Business.objects.create(name="Sample Business")

        # Create a sample image file for the photo field
        image_content = b"Sample image content"
        self.image_file = SimpleUploadedFile("sample_image.jpg", image_content, content_type="image/jpeg")

        self.product = Product.objects.create(
            Business=business,
            product_name="Sample Product",
            price=100,
            Description="This is a sample product description.",
            availability=True,
            photo=self.image_file,
            category="electronics",
            discount=False,
            discounted_price=None,
        )
        user1 = User.objects.create(username="user1")
        user2 = User.objects.create(username="user2")
        Review.objects.create(product=product, rating=4, content="Great product!", created_by=user1)
        Review.objects.create(product=product, rating=5, content="Excellent!", created_by=user2)

    def test_product_str(self):
        # Test the __str__ method
        expected_str = "Sample Product"
        self.assertEqual(str(self.product), expected_str)

    def test_price_field(self):
        # Test valid price
        self.assertEqual(self.product.price, 100)

        # Test negative price (invalid)
        with self.assertRaises(ValidationError):
            invalid_product = Product(
                Business=self.product.Business,
                product_name="Invalid Product",
                price=-50,  # Invalid negative price
                Description="Invalid product description",
                availability=True,
                photo=self.image_file,
                category="fashion_beauty",
                discount=False,
                discounted_price=None,
            )
            invalid_product.full_clean()

    def test_description_field(self):
        # Test valid description
        self.assertEqual(self.product.Description, "This is a sample product description.")

        # Test empty description
        with self.assertRaises(ValidationError):
            invalid_product = Product(
                Business=self.product.Business,
                product_name="Empty Description Product",
                price=75,
                Description="",  # Empty description
                availability=True,
                photo=self.image_file,
                category="home_living",
                discount=False,
                discounted_price=None,
            )
            invalid_product.full_clean()

    def test_availability_field(self):
        # Test valid availability (True)
        self.assertTrue(self.product.availability)

        # Test invalid availability (non-boolean value)
        with self.assertRaises(ValidationError):
            invalid_product = Product(
                Business=self.product.Business,
                product_name="Invalid Availability Product",
                price=120,
                Description="Invalid availability",
                availability="yes",  # Invalid availability (non-boolean value)
                photo=self.image_file,
                category="outdoor",
                discount=False,
                discounted_price=None,
            )
            invalid_product.full_clean()

    def test_category_field(self):
        # Test valid category
        self.assertEqual(self.product.category, "electronics")

        # Test invalid category (not in choices)
        with self.assertRaises(ValidationError):
            invalid_product = Product(
                Business=self.product.Business,
                product_name="Invalid Category Product",
                price=90,
                Description="Invalid category",
                availability=True,
                photo=self.image_file,
                category="invalid_category",  # Invalid category (not in choices)
                discount=False,
                discounted_price=None,
            )
            invalid_product.full_clean()

    def test_discounted_price_field(self):
        # Test valid discounted price (None)
        self.assertIsNone(self.product.discounted_price)

        # Test invalid discounted price (negative value)
        with self.assertRaises(ValidationError):
            invalid_product = Product(
                Business=self.product.Business,
                product_name="Invalid Discounted Price Product",
                price=80,
                Description="Invalid discounted price",
                availability=True,
                photo=self.image_file,
                category="groceries",
                discount=True,
                discounted_price=-10,  # Invalid negative discounted price
            )
            invalid_product.full_clean()
    
    def test_get_rating(self):
        # Calculate the expected average rating (4 + 5) / 2 = 4.5
        expected_rating = 4.5

        # Retrieve the product and call the get_rating method
        product = Product.objects.get(product_name="Sample Product")
        actual_rating = product.get_rating()

        # Check if the calculated rating matches the expected value
        self.assertAlmostEqual(actual_rating, expected_rating, places=2)





class CategoryModelTest(TestCase):
    def setUp(self):
        Category.objects.create(name='electronics')

    def test_category_creation(self):
        """Test the category model can create a category."""
        electronics = Category.objects.get(name='electronics')
        self.assertEqual(electronics.name, 'electronics')

    def test_category_str(self):
        """Test the category model returns the correct string representation."""
        electronics = Category.objects.get(name='electronics')
        self.assertEqual(str(electronics), 'electronics')


class ServiceCategoryModelTest(TestCase):
    def setUp(self):
        # Set up non-modified objects used by all test methods
        Service_category.objects.create(name='IT Services')

    def test_name_label(self):
        # Test the name field label
        service_category = Service_category.objects.get(id=1)
        field_label = service_category._meta.get_field('name').verbose_name
        self.assertEqual(field_label, 'name')

    def test_name_max_length(self):
        # Test the max length of the name field
        service_category = Service_category.objects.get(id=1)
        max_length = service_category._meta.get_field('name').max_length
        self.assertEqual(max_length, 100)

    def test_object_name_is_name(self):
        # Test the __str__ method returns the name
        service_category = Service_category.objects.get(id=1)
        expected_object_name = service_category.name
        self.assertEqual(str(service_category), expected_object_name)

    def test_verbose_name_plural(self):
        # Test the verbose name plural of the model
        service_category = Service_category.objects.get(id=1)
        verbose_name_plural = service_category._meta.verbose_name_plural
        self.assertEqual(verbose_name_plural, 'Service_categories')

from django.test import TestCase
from business.models import Service, Business, Service_category
from datetime import time

class ServiceModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Create instances of foreign key models
        business = Business.objects.create(..., name='Test Business')
        category = Service_category.objects.create(...,name='Test Category')

        # Create a Service instance
        cls.service = Service.objects.create(
            Business=business,
            service_name='Test Service',
            description='Test Description',
            category=category,
            photo='path/to/photo.jpg',
            price=100,
            opening_time=time(9, 0),
            closing_time=time(17, 0),
            location='Test Location'
        )

    def test_service_creation(self):
        # Test the Service model can create a service with the provided attributes
        service = Service.objects.get(id=self.service.id)
        self.assertEqual(service.service_name, 'Test Service')
        self.assertEqual(service.description, 'Test Description')
        self.assertEqual(service.price, 100)
        self.assertEqual(service.opening_time, time(9, 0))
        self.assertEqual(service.closing_time, time(17, 0))
        self.assertEqual(service.location, 'Test Location')

    def test_service_str(self):
        # Test the __str__ method returns the expected string
        self.assertEqual(str(self.service), 'Test Service')

class CustomerModelTest(TestCase):
    def setUp(self):
        # Create a user instance to associate with the customer
        self.user = User.objects.create_user(username='testuser', password='12345')

        # Create a customer instance
        self.customer = Customer.objects.create(
            name='John Doe',
            user=self.user,
            phone='1234567890',
            email='johndoe@example.com',
            Address='123 Main St'
        )

    def test_customer_creation(self):
        # Test that the customer instance has been created
        self.assertIsInstance(self.customer, Customer)

    def test_customer_str(self):
        # Test the __str__ method
        self.assertEqual(str(self.customer), 'John Doe')

    def test_customer_fields(self):
        # Test that the fields contain the correct information
        self.assertEqual(self.customer.name, 'John Doe')
        self.assertEqual(self.customer.user, self.user)
        self.assertEqual(self.customer.phone, '1234567890')
        self.assertEqual(self.customer.email, 'johndoe@example.com')
        self.assertEqual(self.customer.Address, '123 Main St')


class ProfileModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        try:
    
            Profile.objects.create(user_id=1)
        except IntegrityError:
           
            existing_profile = Profile.objects.get(user_id=1)
      
            existing_profile.save()

        # Create a profile for the test_user
        Profile.objects.create(
            user='test_user',
            phone='1234567890',
            email='test@example.com',
            date_created=datetime.today(),
            residence='Test City'
        )

    def test_phone_label(self):
        profile = Profile.objects.get(id=1)
        field_label = profile._meta.get_field('phone').verbose_name
        self.assertEqual(field_label, 'phone')

    def test_email_label(self):
        profile = Profile.objects.get(id=1)
        field_label = profile._meta.get_field('email').verbose_name
        self.assertEqual(field_label, 'email')

    def test_date_created_label(self):
        profile = Profile.objects.get(id=1)
        field_label = profile._meta.get_field('date_created').verbose_name
        self.assertEqual(field_label, 'date created')

    def test_residence_label(self):
        profile = Profile.objects.get(id=1)
        field_label = profile._meta.get_field('residence').verbose_name
        self.assertEqual(field_label, 'residence')

    def test_object_name_is_username(self):
        profile = Profile.objects.get(id=1)
        expected_object_name = profile.user.username
        self.assertEqual(expected_object_name, str(profile))

    def test_phone_max_length(self):
        profile = Profile.objects.get(id=1)
        max_length = profile._meta.get_field('phone').max_length
        self.assertEqual(max_length, 10)

    def test_email_max_length(self):
        profile = Profile.objects.get(id=1)
        max_length = profile._meta.get_field('email').max_length
        self.assertEqual(max_length, 100)

    def test_residence_max_length(self):
        profile = Profile.objects.get(id=1)
        max_length = profile._meta.get_field('residence').max_length
        self.assertEqual(max_length, 150)


class ProfileModelTest(TestCase):
    def test_profile_creation(self):
        # Create a user instance, which should trigger profile creation
        user = User.objects.create_user(username='testuser', password='12345')

        # Fetch the created profile from the database
        profile = Profile.objects.get(user=user)

        # Check that the profile has been created and is linked to the user
        self.assertIsNotNone(profile)
        self.assertEqual(profile.user, user)

class OrderModelTest(TestCase):
    def setUp(self):
        # Create a Product instance
        self.product = Product.objects.create(
            product_name='Test Product',
            price=10.99,
            Description='Test Description',
            availability=True,
            photo='test_photo.jpg',  # Make sure to adjust this to a valid file path
            category='electronics',  # Choose a category from your defined choices
            discount=False,
        )
        
        # Create a Customer instance
        self.customer = Customer.objects.create(name='Test Customer', email='test@example.com')

    def test_order_creation(self):
    # Create an Order instance
        order = Order.objects.create(
            product=self.product,
            customer=self.customer,
            quantity=2,
            address='123 Test St',
            phone='1234567890',
            date=datetime.today().date(),
            status=True
        )
        
        # Check that the order has been created correctly
        self.assertEqual(order.product, self.product)
        self.assertEqual(order.customer, self.customer)
        self.assertEqual(order.quantity, 2)
        self.assertEqual(order.address, '123 Test St')
        self.assertEqual(order.phone, '1234567890')
        self.assertEqual(order.date, datetime.today().date())
        self.assertTrue(order.status)

        # Check the string representation
        self.assertEqual(str(order.product), 'Test Product')


class ContactUsModelTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        # Set up non-modified objects used by all test methods
        Contact_us.objects.create(first_name='Ruth', last_name='Angel', email='blissalexus@gmail.com', mobile='1234567890', message='Hello, this is a test message.')

    def test_first_name_label(self):
        contact = Contact_us.objects.get(id=1)
        field_label = contact._meta.get_field('first_name').verbose_name
        self.assertEqual(field_label, 'first name')

    def test_last_name_label(self):
        contact = Contact_us.objects.get(id=1)
        field_label = contact._meta.get_field('last_name').verbose_name
        self.assertEqual(field_label, 'last name')

    def test_email_label(self):
        contact = Contact_us.objects.get(id=1)
        field_label = contact._meta.get_field('email').verbose_name
        self.assertEqual(field_label, 'email')

    def test_mobile_label(self):
        contact = Contact_us.objects.get(id=1)
        field_label = contact._meta.get_field('mobile').verbose_name
        self.assertEqual(field_label, 'mobile')

    def test_message_label(self):
        contact = Contact_us.objects.get(id=1)
        field_label = contact._meta.get_field('message').verbose_name
        self.assertEqual(field_label, 'message')

    def test_object_name_is_first_name_space_last_name(self):
        contact = Contact_us.objects.get(id=1)
        expected_object_name = f'{contact.first_name} {contact.last_name}'
        self.assertEqual(expected_object_name, str(contact))

    def test_email_field(self):
        contact = Contact_us.objects.get(id=1)
        self.assertEqual(contact.email, 'blissalexus@gmail.com')

    def test_mobile_field(self):
        contact = Contact_us.objects.get(id=1)
        self.assertEqual(contact.mobile, '1234567890')

    def test_message_field(self):
        contact = Contact_us.objects.get(id=1)
        self.assertEqual(contact.message, 'Hello, this is a test message.')



class AppointmentModelTest(TestCase):
    def setUp(self):
        # Create sample data for testing
        business = Business.objects.create(name="Sample Business")
        customer = Customer.objects.create(name="John Doe")
        service = Service.objects.create(name="Haircut")

        self.appointment = Appointment.objects.create(
            Business=business,
            customer=customer,
            service=service,
            date=date(2024, 5, 15),
            time=time(14, 30),
            phone="123-456-7890",
            email="john@example.com",
            status=True,
        )

    def test_appointment_str(self):
        # Test the __str__ method
        expected_str = f"{self.appointment.service} Appointment"
        self.assertEqual(str(self.appointment), expected_str)

    def test_appointment_ordering(self):
        # Test ordering by date and time
        other_appointment = Appointment.objects.create(
            Business=self.appointment.Business,
            customer=self.appointment.customer,
            service=self.appointment.service,
            date=date(2024, 5, 14),
            time=time(10, 0),
            phone="987-654-3210",
            email="jane@example.com",
            status=False,
        )
        appointments = Appointment.objects.all()
        self.assertEqual(appointments[0], other_appointment)
        self.assertEqual(appointments[1], self.appointment)

    def test_phone_field(self):
        # Test valid phone number
        self.assertEqual(self.appointment.phone, "123-456-7890")

        # Test invalid phone number (too short)
        with self.assertRaises(ValidationError):
            invalid_appointment = Appointment(
                Business=self.appointment.Business,
                customer=self.appointment.customer,
                service=self.appointment.service,
                date=date(2024, 5, 20),
                time=time(15, 0),
                phone="123",  # Invalid phone number (too short)
                email="test@example.com",
                status=True,
            )
            invalid_appointment.full_clean()

    def test_email_field(self):
        # Test valid email address
        self.assertEqual(self.appointment.email, "john@example.com")

        # Test invalid email address
        with self.assertRaises(ValidationError):
            invalid_appointment = Appointment(
                Business=self.appointment.Business,
                customer=self.appointment.customer,
                service=self.appointment.service,
                date=date(2024, 5, 21),
                time=time(16, 0),
                phone="987-654-3210",
                email="invalid-email",  # Invalid email address
                status=True,
            )
            invalid_appointment.full_clean()

    def test_default_status(self):
        # Test default status value
        default_appointment = Appointment(
            Business=self.appointment.Business,
            customer=self.appointment.customer,
            service=self.appointment.service,
            date=date(2024, 5, 22),
            time=time(17, 0),
            phone="555-123-4567",
            email="default@example.com",
            # No status specified
        )
        default_appointment.save()
        self.assertFalse(default_appointment.status)


class ReviewModelTest(TestCase):
    def setUp(self):
        # Create sample data for testing
        product = Product.objects.create(name="Sample Product")
        service = Service.objects.create(name="Sample Service")
        user = User.objects.create(username="john_doe")

        self.review = Review.objects.create(
            product=product,
            service=service,
            rating=4,
            content="Great experience!",
            created_by=user,
            created_at=datetime(2024, 5, 15, 14, 30),
        )

    def test_review_str(self):
        # Test the __str__ method
        expected_str = f"{self.product} rating"
        self.assertEqual(str(self.review), expected_str)

    def test_rating_field(self):
        # Test valid rating
        self.assertEqual(self.review.rating, 4)

        # Test invalid rating (out of range)
        with self.assertRaises(ValidationError):
            invalid_review = Review(
                product=self.review.product,
                service=self.review.service,
                rating=6,  # Invalid rating (out of range)
                content="Invalid rating",
                created_by=self.review.created_by,
                created_at=self.review.created_at,
            )
            invalid_review.full_clean()

    def test_content_field(self):
        # Test valid content
        self.assertEqual(self.review.content, "Great experience!")

        # Test empty content
        with self.assertRaises(ValidationError):
            invalid_review = Review(
                product=self.review.product,
                service=self.review.service,
                rating=3,
                content="",  # Empty content
                created_by=self.review.created_by,
                created_at=self.review.created_at,
            )
            invalid_review.full_clean()



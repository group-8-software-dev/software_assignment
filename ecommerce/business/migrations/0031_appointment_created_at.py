# Generated by Django 5.0.3 on 2024-04-28 12:53

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0030_appointment_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='appointment',
            name='created_at',
            field=models.DateField(default=datetime.datetime.today),
        ),
    ]

# Generated by Django 5.0.3 on 2024-03-18 12:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0006_customer_order'),
    ]

    operations = [
        migrations.AddField(
            model_name='service',
            name='photo',
            field=models.ImageField(default='ecommerce/static/no-image.jpeg', upload_to='photos/'),
        ),
    ]

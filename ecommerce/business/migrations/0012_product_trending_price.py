# Generated by Django 5.0.3 on 2024-03-19 10:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('business', '0011_remove_product_trending_price'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='trending_price',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]

from .models import *
from django.core.mail import send_mail


def Confirmation_mail(order_id):
    order = Order.objects.get(id=order_id)
    subject = f'Order number. {order.id}'
    message = f'Dear {order.customer.name},\n\n' \
                f'Thank you for placing an order with us! \n' \
                f'Your order ID is {order.id}.\n' \
                f'Please rate the product ordered \n' \
                f'Payments done upon  delivery of the product(s) \n' \
                f'We appreciate you and we look forward to serving you again soon.'
    mail_sent = send_mail(
        subject,
        message,
        'onestopgroup8@gmail.com',
        [order.email]
    )

    return mail_sent

def Appointment_mail(appointment_id):
    appointment = Appointment.objects.get(id=appointment_id)
    subject = f'Appointment number. {appointment.id}'
    message = f'Dear {appointment.customer.name},\n\n' \
                f'Thank you for booking an appointment with us! \n' \
                f'Your Appointment ID is {appointment.id}.\n' \
                f'Please rate the service \n' \
                f'We appreciate you and we look forward to serving you again soon.'
    mail_sent = send_mail(
        subject,
        message,
        'onestopgroup8@gmail.com',
        [appointment.email]
    )

    return mail_sent

def Approve_appointment_mail(pk):
    appointment = Appointment.objects.get(id=pk)    
    subject = f'Appointment number. {appointment.id}'
    message =   f'Dear {appointment.customer.name},\n \n' \
                f'Your appointment for {appointment.service} on {appointment.date} at {appointment.time} has been approved \n' \
                f'Please rate the service \n' \
                f'We appreciate you and we look forward to serving you again soon.'               
                
    mail_sent = send_mail(
        subject,
        message,
        'onestopgroup8@gmail.com',
        [appointment.email]
    )

    return mail_sent

def alert_mail(business_id):
    business = Business.objects.get(id=business_id)
    subject = f'ALERT ALERT'
    message = f'Dear {business.name},\n\n' \
                f"You have received a new Order or Appointment\n\n" \
                f"Please login to manage your business. visit:https://onestop-w3s1.onrender.com/ \n\n" \
                f"Thank you for using our services"
    
                
    mail_sent = send_mail(
        subject,
        message,
        'onestopgroup8@gmail.com',
        [business.email]
    )

    return mail_sent



def Create4BusinessCustomer(user_id):
    business = Business.objects.get(user=user_id)
    customer = Customer.objects.create(
        name = business.name,
        user = business.user,
        phone = business.business_contact,
        email = business.email,
        Address = business.location
    )
    return customer
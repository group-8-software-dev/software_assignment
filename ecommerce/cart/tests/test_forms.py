from django.test import TestCase
from cart.forms import CartAddProductForm

class CartAddProductFormTest(TestCase):
    def test_valid_cart_add_product_form(self):
        # Example valid form data
        form_data = {
            'quantity': 2,
            'override': False,
        }
        form = CartAddProductForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_empty_fields(self):
        # Test with empty form data
        form_data = {}
        form = CartAddProductForm(data=form_data)
        self.assertFalse(form.is_valid())
        self.assertEqual(len(form.errors), 1)  # Expecting an error for missing 'quantity' field

  

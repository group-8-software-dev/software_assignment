from django.test import TestCase,  RequestFactory
from cart.models import Product, Business, Customer
from cart.views import cart_remove, cart_detail
from cart.views import cart_add
from django.urls import reverse
from django.http import HttpResponseRedirect
from django.contrib.sessions.middleware import SessionMiddleware
from django.contrib.messages.middleware import MessageMiddleware
from django.contrib.auth.models import User
from django.test.client import RequestFactory

from ecommerce import business
from ecommerce.business.views import product
from ecommerce.cart import Cart

class CartAddViewTest(TestCase):
    def setUp(self):
        # Create sample data for testing
        business = Business.objects.create(name="Sample Business")
        product = Product.objects.create(
            Business=business,
            product_name="Sample Product",
            price=100,
            Description="This is a sample product description.",
            availability=True,
            photo=None,
            category="electronics",
            discount=False,
            discounted_price=None,
        )
        customer = Customer.objects.create(name="John Doe")

        # Create a sample user
        self.user = User.objects.create_user(username="testuser", password="testpassword")

        # Create a sample request
        self.factory = RequestFactory()
        self.request = self.factory.post(reverse("cart:cart_add", args=[product.id, business.id]))
        self.request.user = self.user
        middleware = SessionMiddleware()
        middleware.process_request(self.request)
        self.request.session.save()
        self.request._messages = MessageMiddleware(self.request)

        # Add the cart to the request
        self.request.cart = Cart(self.request)

    def test_cart_add_view(self):
        # Set form data
        form_data = {
            "quantity": 2,
            "override": False,
        }

        # Call the view
        response = cart_add(self.request, product.id, business.id)
        self.assertIsInstance(response, HttpResponseRedirect)
        self.assertEqual(response.url, reverse("cart:cart_detail"))

        # Verify that the item was added to the cart
        self.assertEqual(self.request.cart.cart[str(product.id)]["quantity"], 2)




class CartViewTest(TestCase):
    def setUp(self):
        # Create sample data for testing
        business = Business.objects.create(name="Sample Business")
        product = Product.objects.create(
            Business=business,
            product_name="Sample Product",
            price=100,
            Description="This is a sample product description.",
            availability=True,
            photo=None,
            category="electronics",
            discount=False,
            discounted_price=None,
        )
        customer = Customer.objects.create(name="John Doe")

        # Create a sample user
        self.user = User.objects.create_user(username="testuser", password="testpassword")

        # Create a sample request
        self.factory = RequestFactory()
        self.request = self.factory.post(reverse("cart:cart_remove", args=[product.id]))
        self.request.user = self.user
        middleware = SessionMiddleware()
        middleware.process_request(self.request)
        self.request.session.save()
        self.request._messages = MessageMiddleware(self.request)

        # Add the cart to the request
        self.request.cart = Cart(self.request)
        self.request.cart.add(product_id=product.id, business_id=business.id, quantity=2)

    def test_cart_remove_view(self):
        # Call the cart_remove view
        response = cart_remove(self.request, product.id)
        self.assertIsInstance(response, HttpResponseRedirect)
        self.assertEqual(response.url, reverse("cart:cart_detail"))

        # Verify that the item was removed from the cart
        self.assertNotIn(str(product.id), self.request.cart.cart)

    def test_cart_detail_view(self):
        # Call the cart_detail view
        response = cart_detail(self.request)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Sample Product")
        self.assertContains(response, "Quantity")
        self.assertContains(response, "Update Quantity")

# Run the tests: python manage.py test

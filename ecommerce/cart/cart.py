from decimal import Decimal
from django.conf import settings
from business.models import Product
from django.shortcuts import  get_object_or_404

class Cart():
    def __init__(self, request):
        self.session = request.session
        cart = self.session.get(settings.CART_SESSION_ID)
        if not cart:
            cart = self.session[settings.CART_SESSION_ID] = {}
        self.cart = cart

    def add(self,  product_id, business_id, quantity=1, override_quantity=False):       

        product = get_object_or_404(Product, id=product_id)
        product_id = str(product.id)   
        if product.discount:
            price = product.discounted_price
        else:
            price = product.price 

        if product_id not in self.cart:
            self.cart[product_id] = {'quantity':0,
                                     'price':str(price),
                                     'product_id':str(product_id),
                                      'business_id':str(business_id) }
            
        if override_quantity:
            self.cart[product_id]["quantity"] = quantity
        else:
            self.cart[product_id]["quantity"] += quantity
        self.save()

    def save(self):
        self.session.modified = True

    def remove(self, product):
        product_id = str(product.id)
        if product_id in self.cart:
            del self.cart[product_id]
            self.save()

    def __iter__(self):
        "iterate over the items in the cart"
        product_ids = self.cart.keys()
        products = Product.objects.filter(pk__in=product_ids)
        cart = self.cart.copy()
        for product in products:
            cart[str(product.id)]['product']=product
        for item in cart.values():
            item['price'] = Decimal(item['price'])
            item['total_price'] = item['price'] * item['quantity']
            yield item

    def __len__(self):
        "count cart items"
        return sum(item['quantity'] for item in self.cart.values())
    
    def clear(self):
        #remove cart from session
        del self.session[settings.CART_SESSION_ID]
        self.save()

    def get_total_price(self):
        return sum(Decimal(item['price']) * item['quantity'] for item in self)
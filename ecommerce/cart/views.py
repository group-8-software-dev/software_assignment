from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.http import require_POST
from business.models import Product, Business
from .cart import Cart
from .forms import CartAddProductForm
from business.models import *
from business.forms import *

@require_POST
def cart_add(request, product_id, business_id):
    cart = Cart(request)    
    form = CartAddProductForm(request.POST)
    if form.is_valid():
        cd = form.cleaned_data
        cart.add(
                 product_id = product_id, 
                 business_id = business_id,
                 quantity = cd['quantity'],
                 override_quantity=cd['override'])
    return redirect('cart:cart_detail')

@require_POST
def cart_remove(request, product_id):
    cart = Cart(request)
    product = get_object_or_404(Product, id=product_id)
    cart.remove(product)
    return redirect('cart:cart_detail')

def cart_detail(request):
    cart = Cart(request)
    for item in cart:
        item['update_quantity_form'] = CartAddProductForm(initial={
            'quantity':item['quantity'],
            'override':True
        })
    return render (request, 'cart/detail.html', {'cart':cart})


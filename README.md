# OneStop Ecommerce Platform

Welcome to Onestop, a platform that empowers businesses to connect with customers and sell products or services online! Customers can browse a wide variety of offerings, compare prices, and make secure purchases.

Overview
This web platform aims to connect local businesses with their customers, providing a seamless experience for online ordering, delivery, booking services, and secure transactions. Whether you're a restaurant, salon, or fitness studio, this platform empowers you to reach your audience effectively.

Functional Requirements
Business Account
Create Accounts: Businesses can easily create accounts by providing basic information such as name, contact details, and location.
Showcase Offerings: Upload logos, photos to showcase your products or services.
Manage Menus and Listings: Businesses can efficiently manage their menus, service listings, or appointment slots.
Pricing and Availability: Set pricing and availability for your offerings.

Customer Functionality
Browse Businesses: Customers can explore businesses by category, location, or keywords.
Detailed Profiles: View detailed business profiles, service descriptions, and appointment schedules.
Book Appointments: Easily book appointments for services like haircuts, massages, or fitness classes.
Reviews and Ratings: Leave reviews and ratings to help other customers make informed decisions.

Getting Started
For Businesses:
Signup: 
A Business can signup on the website at https://onestop-w3s1.onrender.com.
Once registered, you can access the manage page to add products and services, manage appointments, and view orders.

For Customers:
Signup: 
Create an account to browse and search for listings on the platform.
Browse and Search: Explore businesses by category, location, or keywords.
Ordering and Booking: Add products or services to your cart, checkout securely, and receive confirmation emails for both orders and appointments.


Prerequisites:
Backend -Django Framework
Database -SQLite
Email API -Django Mail
Cloud Hosting -Render Hosting Platform

Installation:
First set up Django in a virtual environment.
Clone the project on the git repo https://gitlab.com/group-8-software-dev/software_assignment.git
Run this python code on the terminal ~ pip install -r requirements.txt to install the requirements needed for this project.










